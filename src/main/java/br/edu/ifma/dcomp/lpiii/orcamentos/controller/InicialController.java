package br.edu.ifma.dcomp.lpiii.orcamentos.controller;

import br.edu.ifma.dcomp.lpiii.orcamentos.view.EmpresaView;
import br.edu.ifma.dcomp.lpiii.orcamentos.view.InicialView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class InicialController implements Initializable {

    @FXML
    private void sair(ActionEvent event) {
        InicialView.stage().close();
    }

    @FXML
    private void editarEmpresa(ActionEvent event) throws IOException {
        new EmpresaView().start(new Stage());
        InicialView.stage().close();
    }

    @FXML
    private void editarUsuario(ActionEvent event) throws IOException {}

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}

