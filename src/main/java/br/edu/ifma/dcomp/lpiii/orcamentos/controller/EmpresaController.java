package br.edu.ifma.dcomp.lpiii.orcamentos.controller;

import br.edu.ifma.dcomp.lpiii.orcamentos.infra.EMFactory;
import br.edu.ifma.dcomp.lpiii.orcamentos.model.Contato;
import br.edu.ifma.dcomp.lpiii.orcamentos.model.Empresa;
import br.edu.ifma.dcomp.lpiii.orcamentos.model.Endereco;
import br.edu.ifma.dcomp.lpiii.orcamentos.repository.EmpresaRepository;
import br.edu.ifma.dcomp.lpiii.orcamentos.view.InicialView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import javax.persistence.EntityManager;
import java.net.URL;
import java.util.ResourceBundle;

public class EmpresaController implements Initializable {

    @FXML
    private TextField nome_empresa;
    @FXML
    private TextField responsavel_empresa;
    @FXML
    private TextField nota_empresa;
    @FXML
    private TextField logradouro_endereco_empresa;
    @FXML
    private TextField cep_endereco_empresa;
    @FXML
    private TextField cidade_endereco_empresa;
    @FXML
    private TextField bairro_endereco_empresa;
    @FXML
    private TextField numero_endereco;
    @FXML
    private ComboBox<String> uf_endereco_empresa;
    @FXML
    private TextField telefone_empresa;
    @FXML
    private TextField celular_empresa;
    @FXML
    private TextField email_empresa;
    @FXML
    private TextField site_empresa;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    public void salvarEmpresa() {
        Empresa empresa = new Empresa();
        empresa.setNome(nome_empresa.getText());
        empresa.setResponsavel(responsavel_empresa.getText());
        empresa.setNota(Double.parseDouble(nota_empresa.getText()));

        Endereco endereco = new Endereco();
        endereco.setLogradouro(logradouro_endereco_empresa.getText());
        endereco.setCep(cep_endereco_empresa.getText());
        endereco.setBairro(bairro_endereco_empresa.getText());
        endereco.setCidade(cidade_endereco_empresa.getText());
        endereco.setNumero(Integer.parseInt(numero_endereco.getText()));
        endereco.setUf("MA");

        Contato contato = new Contato();
        contato.setTelefone(telefone_empresa.getText());
        contato.setCelular(celular_empresa.getText());
        contato.setEmail(email_empresa.getText());
        contato.setSite(site_empresa.getText());
        empresa.setContato(contato);

        EntityManager entityManager = EMFactory.getEntityManager();
        EmpresaRepository empresaRepository = new EmpresaRepository(entityManager);

        entityManager.getTransaction().begin();
        empresaRepository.salva(empresa);
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    @FXML
    public void funcionarios() {

    }

}

