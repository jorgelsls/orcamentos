package br.edu.ifma.dcomp.lpiii.orcamentos.main;

import br.edu.ifma.dcomp.lpiii.orcamentos.infra.EMFactory;
import br.edu.ifma.dcomp.lpiii.orcamentos.view.LoginView;

public class Main {

    public static void main(String[] args) {
        EMFactory.getEntityManager();

        LoginView.main(args);

        EMFactory.closeEntityManager();
        EMFactory.closeEntityManagerFactory();
    }

}
