package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import javax.persistence.EntityManager;

class GenericRepository<T> {

    private final EntityManager manager;

    GenericRepository(EntityManager manager) {
        this.manager = manager;
    }

    void salva(T t) {
        this.manager.merge(t );
    }

    T buscaPorId(Class<T> clazz, Integer id) {
        return this.manager.find(clazz, id);
    }

}
