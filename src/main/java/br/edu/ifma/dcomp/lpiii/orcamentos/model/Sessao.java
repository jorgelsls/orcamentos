package br.edu.ifma.dcomp.lpiii.orcamentos.model;

import java.util.HashMap;
import java.util.Map;

public class Sessao {

    final private static Map<String, Usuario> sessao = new HashMap<>();

    public static Usuario get(String chave) {
        if (!sessao.containsKey(chave)) {
            throw new RuntimeException();
        }

        return sessao.get(chave);
    }

    public static void atualiza(String chave, Usuario valor) {
        sessao.put(chave, valor);
    }
}
