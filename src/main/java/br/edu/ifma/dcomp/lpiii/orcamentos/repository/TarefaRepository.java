package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import br.edu.ifma.dcomp.lpiii.orcamentos.model.Tarefa;

import javax.persistence.EntityManager;

public class TarefaRepository {

    private EntityManager manager;
    final private GenericRepository<Tarefa> genericRepository;

    public TarefaRepository(EntityManager manager) {
        this.manager = manager;
        this.genericRepository = new GenericRepository<>(this.manager );
    }

    public void salva(Tarefa tarefa) {
        this.genericRepository.salva(tarefa );
    }

    public Tarefa buscaPor(Integer id) {
        return this.genericRepository.buscaPorId(Tarefa.class, id);
    }

}
