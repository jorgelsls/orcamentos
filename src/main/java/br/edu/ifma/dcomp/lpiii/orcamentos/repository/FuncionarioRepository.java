package br.edu.ifma.dcomp.lpiii.orcamentos.repository;

import br.edu.ifma.dcomp.lpiii.orcamentos.model.Funcionario;

import javax.persistence.EntityManager;

public class FuncionarioRepository {

    private EntityManager manager;
    final private GenericRepository<Funcionario> genericRepository;

    public FuncionarioRepository(EntityManager manager) {
        this.manager = manager;
        this.genericRepository = new GenericRepository<>(this.manager );
    }

    public void salva(Funcionario funcionario) {
        this.genericRepository.salva(funcionario );
    }

    public Funcionario buscaPor(Integer id) {
        return this.genericRepository.buscaPorId(Funcionario.class, id);
    }

}
